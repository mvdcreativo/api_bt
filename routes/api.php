<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Auth\PermissionController;
use App\Http\Controllers\Api\Auth\RoleController;
use App\Http\Controllers\Api\CancerPatientAntecedentController;
use App\Http\Controllers\Api\CityController;
use App\Http\Controllers\Api\ConsultaCguController;
use App\Http\Controllers\Api\CountryController;
use App\Http\Controllers\Api\DashboardController;
use App\Http\Controllers\Api\DgController;
use App\Http\Controllers\Api\DoctorController;
use App\Http\Controllers\Api\DocumentController;
use App\Http\Controllers\Api\EstadioController;
use App\Http\Controllers\Api\EvolutionController;
use App\Http\Controllers\Api\FamilyController;
use App\Http\Controllers\Api\FinancingController;
use App\Http\Controllers\Api\GenController;
use App\Http\Controllers\Api\ClasificationController;
use App\Http\Controllers\Api\GenResultSpecController;
use App\Http\Controllers\Api\InternationalBdController;
use App\Http\Controllers\Api\KinshipController;
use App\Http\Controllers\Api\LabController;
use App\Http\Controllers\Api\LocationController;
use App\Http\Controllers\Api\MedicalInstitutionController;
use App\Http\Controllers\Api\NewTumorController;
use App\Http\Controllers\Api\PatientController;
use App\Http\Controllers\Api\PatientScoreController;
use App\Http\Controllers\Api\QuestionnaireController;
use App\Http\Controllers\Api\ResultController;
use App\Http\Controllers\Api\ResultSpecController;
use App\Http\Controllers\Api\ResultStudyTypeController;
use App\Http\Controllers\Api\SampleController;
use App\Http\Controllers\Api\StageController;
use App\Http\Controllers\Api\StateController;
use App\Http\Controllers\Api\TnmController;
use App\Http\Controllers\Api\TopographyController;
use App\Http\Controllers\Api\TumorLineageController;
use App\Http\Controllers\Api\TypeSampleController;
use App\Http\Controllers\Api\TypeSurgeryController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\TracePatientController;
use App\Http\Controllers\Api\TraceSampleController;
use App\Http\Controllers\Api\StorageSampleController;
use App\Http\Controllers\Api\StudyController;
use App\Http\Controllers\Api\StudyRequestController;
use App\Http\Controllers\Api\StudyTypeController;
use App\Http\Controllers\Api\TracingController;
use App\Http\Controllers\Api\TypeLocationController;
use App\Models\NewTumors;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//PROTECTED ROUTES
Route::middleware('auth:sanctum')->group(function () {

    Route::apiResources([
        'type_samples' => TypeSampleController::class,
        'topographies' => TopographyController::class,
        'tumor_lineages' => TumorLineageController::class,
        'countries' => CountryController::class,
        'states' => StateController::class,
        'cities' => CityController::class,
        'patients' => PatientController::class,
        'medical-institutions' => MedicalInstitutionController::class,
        'doctors' => DoctorController::class,
        'surgeries' => TypeSurgeryController::class,
        'samples' => SampleController::class,
        'tnms' => TnmController::class,
        'families' => FamilyController::class,
        'estadios' => EstadioController::class,
        'users' => UserController::class,
        'stages' => StageController::class,
        'documents' => DocumentController::class,
        'evolutions' => EvolutionController::class,
        'traces_samples' => TraceSampleController::class,
        'traces_patients' => TracePatientController::class,
        'type_locations' => TypeLocationController::class,
        'locations_samples' => LocationController::class,
        'storage_samples' => StorageSampleController::class,
        'cancer_patient_antecedent' => CancerPatientAntecedentController::class,
        'consulta_cgus' => ConsultaCguController::class,
        'dgs' => DgController::class,
        'kinships' => KinshipController::class,
        'new_tomors' => NewTumorController::class,
        'questionnaires' => QuestionnaireController::class,
        'tracings' => TracingController::class,
        'international_bds' => InternationalBdController::class,
        'studies' => StudyController::class,
        'gens' => GenController::class,
        'clasifications' => ClasificationController::class,
        'labs_result' => LabController::class,
        'financings' => FinancingController::class,
        'study_types' => StudyTypeController::class,
        'result_study_types' => ResultStudyTypeController::class,
        'results' => ResultController::class,
        'patient_scores' => PatientScoreController::class,
        'study_requests' => StudyRequestController::class,

    ]);

    //Check exist
    Route::get('check_patient_exist', [PatientController::class, 'check_patient_exist']);
    Route::get('last_id', [PatientController::class, 'last_id']);
    Route::get('last_id_sample', [SampleController::class, 'last_id_sample']);
    Route::get('check_sample_exist', [SampleController::class, 'check_sample_exist']);
    Route::get('check_email_exist', [UserController::class, 'check_email_exist']);
    Route::get('check_role_exist', [RoleController::class, 'check_role_exist']);
    Route::get('not_available_positions', [StorageSampleController::class, 'not_available_positions']);


    Route::post('clone_sample/{id}', [SampleController::class, 'clone_sample']);
    Route::get('gen_code_sample', [SampleController::class, 'genCodeSample']);
    Route::get('code_sample_exist', [SampleController::class, 'code_sample_exist']);
    Route::get('download_document/{id}', [DocumentController::class, 'download']);
    Route::post('family_remove_member/{id}', [FamilyController::class, 'removeMember']);
    Route::get('data_dash', [DashboardController::class, 'index']);
    Route::get('data_dash_samples_end', [DashboardController::class, 'samples_end']);
    Route::get('data_dash_samples_in_process', [DashboardController::class, 'samples_in_process']);

    Route::post('patient_international_bds', [InternationalBdController::class, 'addPatient']);
    Route::post('patient_international_bds/{id}', [InternationalBdController::class, 'removePatient']);
    Route::post('patient_study', [StudyController::class, 'addPatient']);
    Route::post('patient_study/{id}', [StudyController::class, 'removePatient']);
    Route::post('patient_questionary', [QuestionnaireController::class, 'addPatient']);
    Route::post('patient_questionary/{id}', [QuestionnaireController::class, 'removePatient']);

    //Roles y permisos
    Route::group(['prefix' => 'admin'], function () {
        Route::apiResources([
            'roles'=> RoleController::class,
            'permissions'=> PermissionController::class
        ]);
        Route::post('assign_permission_to_role/{role_id}', [RoleController::class, 'assign_permission']);

    });



    //auth
    Route::group(['prefix' => 'auth'], function () {
        Route::post('user', [AuthController::class, 'currentUser']);
        Route::get('logout', [AuthController::class, 'logout']);

    });
});


//PUBLIC ROUTES
Route::group(['prefix' => 'auth'], function () {
    Route::post('signup', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);
});
