<?php

namespace App\Observers;

use App\Models\Sample;

class SampleObserver
{
    // public $afterCommit = true;

    public function created(Sample $sample)
    {
        //
    }

    /**
     * Handle the Sample "updated" event.
     *
     * @param  \App\Models\Sample  $sample
     * @return void
     */
    public function updated(Sample $sample)
    {


    }

    /**
     * Handle the Sample "deleted" event.
     *
     * @param  \App\Models\Sample  $sample
     * @return void
     */
    public function deleted(Sample $sample)
    {
        //
    }

    /**
     * Handle the Sample "restored" event.
     *
     * @param  \App\Models\Sample  $sample
     * @return void
     */
    public function restored(Sample $sample)
    {
        //
    }

    /**
     * Handle the Sample "force deleted" event.
     *
     * @param  \App\Models\Sample  $sample
     * @return void
     */
    public function forceDeleted(Sample $sample)
    {
        //
    }
}
