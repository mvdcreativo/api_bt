<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TypeSample;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class TypeSampleController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = TypeSample::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        $types_samples = $query
        ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($types_samples,'TypeSample list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $typeSample = TypeSample::create($input);

        return $this->successResponse($typeSample,'TypeSample saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypeSample  $typeSample
     * @return \Illuminate\Http\Response
     */
    public function show(TypeSample $typeSample)
    {

        $typeSample_show = TypeSample::find($typeSample->id);

        if (empty($typeSample_show)) {
            return $this->errorResponse('TypeSample not found',404);
        }
        return $this->successResponse($typeSample_show,'TypeSample show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TypeSample  $typeSample
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TypeSample $typeSample)
    {
        $typeSample_update = TypeSample::find($typeSample->id);

        if (empty($typeSample_update)) {
            return $this->errorResponse('TypeSample not found',404);
        }
        $typeSample_update->fill($request->all());
        $typeSample_update->save();

        return $this->successResponse($typeSample_update,'TypeSample updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeSample  $typeSample
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypeSample $typeSample)
    {
        $typeSample_delete = TypeSample::find($typeSample->id);

        if(isset($typeSample_delete->samples) && count($typeSample_delete->samples))
        abort(500, "actives_relations");

        if (empty($typeSample_delete)) {
            return $this->errorResponse('TypeSample not found',404);
        }
        $typeSample_delete->delete();
        return $this->successResponse($typeSample_delete,'TypeSample deleted', 200);
    }
}
