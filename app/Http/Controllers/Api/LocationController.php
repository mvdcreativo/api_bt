<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Location;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class LocationController extends Controller
{

    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Location::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }


        $locations = $query
        ->with('type_location')
        // ->filter($filter)
        ->orderBy('created_at', $sort)
        ->paginate($per_page);

        return $this->successResponse($locations,'Locations list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $location = Location::create($input);

        return $this->successResponse($location,'Location saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $location_show = Location::with('type_location')->find($id);

        if (empty($location_show)) {
            return $this->errorResponse('Location not found',404);
        }
        return $this->successResponse($location_show,'Location show', 200);    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $location_update = Location::find($id);

        if (empty($location_update)) {
            return $this->errorResponse('Location not found',404);
        }
        $location_update->fill($request->all());
        $location_update->save();
        $location_update->type_location;

        return $this->successResponse($location_update,'Location updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location_delete = Location::find($id);

        if(isset($location_delete->storage_sample) && count($location_delete->storage_sample))
        abort(500, "actives_relations");

        if (empty($location_delete)) {
            return $this->errorResponse('Location not found',404);
        }
        $location_delete->delete();
        return $this->successResponse($location_delete,'Location deleted', 200);
    }
}
