<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\StorageSample;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class StorageSampleController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = StorageSample::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        if ($request->get('patient_id')) {
            $patient_id = $request->get('patient_id');
        }else{
            $patient_id = "";
        }

        $storageSamples = $query
        ->with('location','sample')
        ->patient_id($patient_id)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($storageSamples,'StorageSample list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $storageSample = StorageSample::create($input);

        return $this->successResponse($storageSample,'StorageSample saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StorageSample  $storageSample
     * @return \Illuminate\Http\Response
     */
    public function show(StorageSample $storageSample)
    {

        $storageSample_show = StorageSample::find($storageSample->id);

        if (empty($storageSample_show)) {
            return $this->errorResponse('StorageSample not found',404);
        }
        return $this->successResponse($storageSample_show,'StorageSample show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StorageSample  $storageSample
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StorageSample $storageSample)
    {
        $storageSample_update = StorageSample::find($storageSample->id);

        if (empty($storageSample_update)) {
            return $this->errorResponse('StorageSample not found',404);
        }
        $storageSample_update->fill($request->all());
        $storageSample_update->save();

        return $this->successResponse($storageSample_update,'StorageSample updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StorageSample  $storageSample
     * @return \Illuminate\Http\Response
     */
    public function destroy(StorageSample $storageSample)
    {
        $storageSample_delete = StorageSample::find($storageSample->id);
        if (empty($storageSample_delete)) {
            return $this->errorResponse('StorageSample not found',404);
        }
        $storageSample_delete->delete();
        return $this->successResponse($storageSample_delete,'StorageSample deleted', 200);
    }

    public function not_available_positions(Request $request)
    {
        $code = $request->get('code');
        $query = StorageSample::query();
        $not_available_positions = $query
        ->select('code')
        ->where('code','LIKE', $code.'%')
        ->get();

        return $not_available_positions;

    }
}
