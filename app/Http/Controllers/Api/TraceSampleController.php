<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Sample;
use App\Models\TraceSample;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class TraceSampleController extends Controller
{

    use ApiResponser;


    public function index(Request $request)
    {
        $query = TraceSample::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }


        $traces = $query
        ->with('stage')
        ->sample_id($request->get('sample_id'))
        ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($traces,'Trace list', 200);
    }


    public function store(Request $request)
    {
        $trace = new TraceSample;
        $trace->fill($request->all());
        $trace->save();
        $trace->stage;

        $stage=  $trace->stage;
        if(!empty($stage)){
            $sample = Sample::find($trace->sample_id);
            if (!empty($sample)){
                $sample->last_stage_status = $stage->end;
                $sample->save();
            }
        }
        // return $trace;
        return $this->successResponse($trace,'Trace created', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TraceSample  $traceSample
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trace = TraceSample::find($id);
        return $this->successResponse($trace,'Trace view', 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TraceSample  $traceSample
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $trace = TraceSample::with('stage')->find($id);

        if (empty($trace)) {
            return $this->errorResponse('TraceSample not found',404);
        }
        $trace->fill($request->all());
        $trace->save();
        $stage=  $trace->stage;
        if(!empty($stage)){
            $sample = Sample::find($trace->sample_id);
            if (!empty($sample)){
                $sample->last_stage_status = $stage->end;
                $sample->save();
            }
        }

        return $this->successResponse($trace,'TraceSample updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TraceSample  $traceSample
     * @return \Illuminate\Http\Response
     */
    public function destroy(TraceSample $traceSample)
    {
        //
    }
}
