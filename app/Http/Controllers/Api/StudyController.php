<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Patient;
use App\Models\PatientStudy;
use App\Models\Study;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class StudyController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Study::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }


        $studies = $query
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($studies,'Study list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $study = Study::create($input);

        return $this->successResponse($study,'Study saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Study  $study
     * @return \Illuminate\Http\Response
     */
    public function show(Study $study)
    {

        $study_show = Study::find($study->id);

        if (empty($study_show)) {
            return $this->errorResponse('Study not found',404);
        }
        return $this->successResponse($study_show,'Study show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Study  $study
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Study $study)
    {
        $study_update = Study::find($study->id);

        if (empty($study_update)) {
            return $this->errorResponse('Study not found',404);
        }
        $study_update->fill($request->all());
        $study_update->save();

        return $this->successResponse($study_update,'Study updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Study  $study
     * @return \Illuminate\Http\Response
     */
    public function destroy(Study $study)
    {
        $study_delete = Study::find($study->id);
        if(isset($study_delete->sample_data_anatomos) && count($study_delete->sample_data_anatomos))
        abort(500, "actives_relations");

        if (empty($study_delete)) {
            return $this->errorResponse('Study not found',404);
        }
        $study_delete->delete();
        return $this->successResponse($study_delete,'Study deleted', 200);
    }




    public function addPatient(Request $request)
    {

        $patient_id= $request->get('patient_id');

        // return $input;
        $relation = PatientStudy::create($request->all());
        if($relation){
            $patient = Patient::find($patient_id);
        }

        // $newItem = BdInternat ionalPatient::create($input);
        return $this->successResponse($patient->studies,'Created Patient Study relationship', 201);

    }

    public function removePatient($id, Request $request)
    {
        $relation = PatientStudy::find($id);

        if (empty($relation)) {
            return $this->errorResponse('Patient Study relationship not found',404);
        }
        $relation->delete();

        if($relation){
            $patient = Patient::find($request->get('patient_id'));
            return $this->successResponse($patient->studies,'relationship deleted', 200);

        }
    }
}
