<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BdInternationalPatient;
use App\Models\InternationalBd;
use App\Models\Patient;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class InternationalBdController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = InternationalBd::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        $international_bds = $query
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($international_bds,'InternationalBd list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $international_bd = InternationalBd::create($input);

        return $this->successResponse($international_bd,'InternationalBd saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InternationalBd  $international_bd
     * @return \Illuminate\Http\Response
     */
    public function show(InternationalBd $international_bd)
    {

        $international_bd_show = InternationalBd::find($international_bd->id);

        if (empty($international_bd_show)) {
            return $this->errorResponse('InternationalBd not found',404);
        }
        return $this->successResponse($international_bd_show,'InternationalBd show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InternationalBd  $international_bd
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InternationalBd $international_bd)
    {
        $international_bd_update = InternationalBd::find($international_bd->id);

        if (empty($international_bd_update)) {
            return $this->errorResponse('InternationalBd not found',404);
        }
        $international_bd_update->fill($request->all());
        $international_bd_update->save();

        return $this->successResponse($international_bd_update,'InternationalBd updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InternationalBd  $international_bd
     * @return \Illuminate\Http\Response
     */
    public function destroy(InternationalBd $international_bd)
    {
        $international_bd_delete = InternationalBd::find($international_bd->id);
        if(isset($international_bd_delete->sample_data_anatomos) && count($international_bd_delete->sample_data_anatomos))
        abort(500, "actives_relations");

        if (empty($international_bd_delete)) {
            return $this->errorResponse('InternationalBd not found',404);
        }
        $international_bd_delete->delete();
        return $this->successResponse($international_bd_delete,'InternationalBd deleted', 200);
    }


    public function addPatient(Request $request)
    {

        $patient_id= $request->get('patient_id');

        // return $input;
        $relation = BdInternationalPatient::create($request->all());
        if($relation){
            $patient = Patient::find($patient_id);
        }

        // $newItem = BdInternat ionalPatient::create($input);
        return $this->successResponse($patient->international_bds,'Patien BdInternational relationship', 201);

    }

    public function removePatient($id, Request $request)
    {
        $relation = BdInternationalPatient::find($id);

        if (empty($relation)) {
            return $this->errorResponse('Patien BdInternational relationship not found',404);
        }
        $relation->delete();

        if($relation){
            $patient = Patient::find($request->get('patient_id'));
            return $this->successResponse($patient->international_bds,'relationship deleted', 200);

        }
    }

}
