<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Dg;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class DgController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Dg::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        $dgs = $query
        ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($dgs,'Dg list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $dg = Dg::create($input);

        return $this->successResponse($dg,'Dg saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dg  $dg
     * @return \Illuminate\Http\Response
     */
    public function show(Dg $dg)
    {

        $dg_show = Dg::find($dg->id);

        if (empty($dg_show)) {
            return $this->errorResponse('Dg not found',404);
        }
        return $this->successResponse($dg_show,'Dg show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dg  $dg
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dg $dg)
    {
        $dg_update = Dg::find($dg->id);

        if (empty($dg_update)) {
            return $this->errorResponse('Dg not found',404);
        }
        $dg_update->fill($request->all());
        $dg_update->save();

        return $this->successResponse($dg_update,'Dg updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dg  $dg
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dg $dg)
    {
        $dg_delete = Dg::find($dg->id);
        if(isset($dg_delete->patients) && count($dg_delete->patients))
        abort(500, "actives_relations");
        if (empty($dg_delete)) {
            return $this->errorResponse('Dg not found',404);
        }
        $dg_delete->delete();
        return $this->successResponse($dg_delete,'Dg deleted', 200);
    }
}
