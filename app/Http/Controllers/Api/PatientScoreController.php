<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\PatientScore;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class PatientScoreController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = PatientScore::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        $patient_scores = $query
        ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($patient_scores,'PatientScore list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $patient_score = PatientScore::create($input);

        return $this->successResponse($patient_score,'PatientScore saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PatientScore  $patient_score
     * @return \Illuminate\Http\Response
     */
    public function show(PatientScore $patient_score)
    {

        $patient_score_show = PatientScore::find($patient_score->id);

        if (empty($patient_score_show)) {
            return $this->errorResponse('PatientScore not found',404);
        }
        return $this->successResponse($patient_score_show,'PatientScore show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PatientScore  $patient_score
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PatientScore $patient_score)
    {
        $patient_score_update = PatientScore::find($patient_score->id);

        if (empty($patient_score_update)) {
            return $this->errorResponse('PatientScore not found',404);
        }
        $patient_score_update->fill($request->all());
        $patient_score_update->save();

        return $this->successResponse($patient_score_update,'PatientScore updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PatientScore  $patient_score
     * @return \Illuminate\Http\Response
     */
    public function destroy(PatientScore $patient_score)
    {
        $patient_score_delete = PatientScore::find($patient_score->id);
        if(isset($patient_score_delete->sample_data_anatomos) && count($patient_score_delete->sample_data_anatomos))
        abort(500, "actives_relations");

        if (empty($patient_score_delete)) {
            return $this->errorResponse('PatientScore not found',404);
        }
        $patient_score_delete->delete();
        return $this->successResponse($patient_score_delete,'PatientScore deleted', 200);
    }
}
