<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Result;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Result::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        if ($request->get('patient_id')) {
            $patient_id = $request->get('patient_id');
        }else{
            $patient_id = "";
        }

        if ($request->get('sample_id')) {
            $saple_id = $request->get('sample_id');
        }else{
            $saple_id = "";
        }

        $results = $query
        ->with('financing','lab','sample','study_type','gens_results', 'clasifications_results', 'result_study_type', 'study_request')
        ->filter($filter)
        ->sampleId($saple_id)
        ->patientId($patient_id)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($results,'Result list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();

        $result = Result::create($input);
        $gens_results = $request->get('gens_results');
        $result->gens_results()->attach($gens_results);

        $result = Result::with('financing','lab','study_type','gens_results', 'clasifications_results', 'result_study_type', 'study_request')->find($result->id);


        return $this->successResponse($result,'Result saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function show(Result $result)
    {

        $result_show = Result::with('lab','financing', 'study_type','gens_results', 'clasifications_results', 'result_study_type', 'study_request')->find($result->id);

        if (empty($result_show)) {
            return $this->errorResponse('Result not found',404);
        }

        return $this->successResponse($result_show,'Result show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Result $result)
    {
        $result_update = Result::find($result->id);

        if (empty($result_update)) {
            return $this->errorResponse('Result not found',404);
        }


        $result_update->fill($request->all());

        $gens_results = $request->get('gens_results');
        $result->gens_results()->detach();
        $result_update->gens_results()->attach($gens_results);

        $result_update->save();
        $result_update = Result::with('lab','financing','gens_results', 'study_type', 'clasifications_results', 'result_study_type', 'study_request' )->find($result->id);

        return $this->successResponse($result_update,'Result updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function destroy(Result $result)
    {
        $result_delete = Result::find($result->id);
        if(isset($result_delete->patients) && count($result_delete->patients))
        abort(500, "actives_relations");
        if (empty($result_delete)) {
            return $this->errorResponse('Result not found',404);
        }
        $result_delete->delete();
        return $this->successResponse($result_delete,'Result deleted', 200);
    }
}
