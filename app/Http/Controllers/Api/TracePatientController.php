<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TracePatient;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class TracePatientController extends Controller
{

    use ApiResponser;


    public function index(Request $request)
    {
        $query = TracePatient::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }


        $traces = $query
        ->with('evolution')
        ->patient_id($request->get('patient_id'))
        ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($traces,'Trace list', 200);
    }


    public function store(Request $request)
    {
        $trace = new TracePatient;
        $trace->fill($request->all());
        $trace->save();
        $trace->evolution;
        // return $trace;
        return $this->successResponse($trace,'Trace created', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TracePatient  $tracePatient
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trace = TracePatient::find($id);
        return $this->successResponse($trace,'Trace view', 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TracePatient  $tracePatient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $trace = TracePatient::with('evolution')->find($id);

        if (empty($trace)) {
            return $this->errorResponse('TracePatient not found',404);
        }
        $trace->fill($request->all());
        $trace->save();

        return $this->successResponse($trace,'TracePatient updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TracePatient  $tracePatient
     * @return \Illuminate\Http\Response
     */
    public function destroy(TracePatient $tracePatient)
    {
        //
    }
}
