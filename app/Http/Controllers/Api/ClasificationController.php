<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Clasification;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class ClasificationController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Clasification::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        $clasifications = $query
        ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($clasifications,'Clasification list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $clasification = Clasification::create($input);

        return $this->successResponse($clasification,'Clasification saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Clasification  $clasification
     * @return \Illuminate\Http\Response
     */
    public function show(Clasification $clasification)
    {

        $clasification_show = Clasification::find($clasification->id);

        if (empty($clasification_show)) {
            return $this->errorResponse('Clasification not found',404);
        }
        return $this->successResponse($clasification_show,'Clasification show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Clasification  $clasification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Clasification $clasification)
    {
        $clasification_update = Clasification::find($clasification->id);

        if (empty($clasification_update)) {
            return $this->errorResponse('Clasification not found',404);
        }
        $clasification_update->fill($request->all());
        $clasification_update->save();

        return $this->successResponse($clasification_update,'Clasification updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Clasification  $clasification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clasification $clasification)
    {
        $clasification_delete = Clasification::find($clasification->id);
        if(isset($clasification_delete->patients) && count($clasification_delete->patients))
        abort(500, "actives_relations");
        if (empty($clasification_delete)) {
            return $this->errorResponse('Clasification not found',404);
        }
        $clasification_delete->delete();
        return $this->successResponse($clasification_delete,'Clasification deleted', 200);
    }
}
