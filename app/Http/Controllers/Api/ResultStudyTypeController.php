<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ResultStudyType;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class ResultStudyTypeController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = ResultStudyType::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        if ($request->get('study_type_id')) {
            $study_type_id = $request->get('study_type_id');
        }else{
            $study_type_id = "";
        }

        $resultStudyTypes = $query
        ->where('study_type_id', $study_type_id )
        ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($resultStudyTypes,'ResultStudyType list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $resultStudyType = ResultStudyType::create($input);

        return $this->successResponse($resultStudyType,'ResultStudyType saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ResultStudyType  $resultStudyType
     * @return \Illuminate\Http\Response
     */
    public function show(ResultStudyType $resultStudyType)
    {

        $resultStudyType_show = ResultStudyType::find($resultStudyType->id);

        if (empty($resultStudyType_show)) {
            return $this->errorResponse('ResultStudyType not found',404);
        }
        return $this->successResponse($resultStudyType_show,'ResultStudyType show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ResultStudyType  $resultStudyType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResultStudyType $resultStudyType)
    {
        $resultStudyType_update = ResultStudyType::find($resultStudyType->id);

        if (empty($resultStudyType_update)) {
            return $this->errorResponse('ResultStudyType not found',404);
        }
        $resultStudyType_update->fill($request->all());
        $resultStudyType_update->save();

        return $this->successResponse($resultStudyType_update,'ResultStudyType updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ResultStudyType  $resultStudyType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResultStudyType $resultStudyType)
    {
        $resultStudyType_delete = ResultStudyType::find($resultStudyType->id);
        if(isset($resultStudyType_delete->patients) && count($resultStudyType_delete->patients))
        abort(500, "actives_relations");
        if (empty($resultStudyType_delete)) {
            return $this->errorResponse('ResultStudyType not found',404);
        }
        $resultStudyType_delete->delete();
        return $this->successResponse($resultStudyType_delete,'ResultStudyType deleted', 200);
    }
}
