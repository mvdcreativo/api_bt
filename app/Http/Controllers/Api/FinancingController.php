<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Financing;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class FinancingController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Financing::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        $financings = $query
        ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($financings,'Financing list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $financing = Financing::create($input);

        return $this->successResponse($financing,'Financing saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Financing  $financing
     * @return \Illuminate\Http\Response
     */
    public function show(Financing $financing)
    {

        $financing_show = Financing::find($financing->id);

        if (empty($financing_show)) {
            return $this->errorResponse('Financing not found',404);
        }
        return $this->successResponse($financing_show,'Financing show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Financing  $financing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Financing $financing)
    {
        $financing_update = Financing::find($financing->id);

        if (empty($financing_update)) {
            return $this->errorResponse('Financing not found',404);
        }
        $financing_update->fill($request->all());
        $financing_update->save();

        return $this->successResponse($financing_update,'Financing updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Financing  $financing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Financing $financing)
    {
        $financing_delete = Financing::find($financing->id);
        if(isset($financing_delete->patients) && count($financing_delete->patients))
        abort(500, "actives_relations");
        if (empty($financing_delete)) {
            return $this->errorResponse('Financing not found',404);
        }
        $financing_delete->delete();
        return $this->successResponse($financing_delete,'Financing deleted', 200);
    }
}
