<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\NewTumor;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class NewTumorController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = NewTumor::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        $new_tumors = $query
        ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($new_tumors,'NewTumor list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $new_tumor = NewTumor::with('topography')->create($input);

        $new_tumor->topography;
        return $this->successResponse($new_tumor,'NewTumor saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NewTumor  $new_tumor
     * @return \Illuminate\Http\Response
     */
    public function show(NewTumor $new_tumor)
    {

        $new_tumor_show = NewTumor::with('topography')->find($new_tumor->id);

        if (empty($new_tumor_show)) {
            return $this->errorResponse('NewTumor not found',404);
        }

        $new_tumor->topography;

        return $this->successResponse($new_tumor_show,'NewTumor show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NewTumor  $new_tumor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NewTumor $new_tumor)
    {
        $new_tumor_update = NewTumor::with('topography')->find($new_tumor->id);

        if (empty($new_tumor_update)) {
            return $this->errorResponse('NewTumor not found',404);
        }
        $new_tumor_update->fill($request->all());
        $new_tumor_update->save();

        $new_tumor->topography;

        return $this->successResponse($new_tumor_update,'NewTumor updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NewTumor  $new_tumor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $new_tumor_delete = NewTumor::find($id);

        if (empty($new_tumor_delete)) {
            return $this->errorResponse('NewTumor not found',404);
        }
        $new_tumor_delete->delete();
        return $this->successResponse($new_tumor_delete,'NewTumor deleted', 200);
    }
}
