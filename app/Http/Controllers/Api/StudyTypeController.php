<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\StudyType;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class StudyTypeController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = StudyType::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        $studyTypes = $query
        ->with('study_type_results')
        ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($studyTypes,'StudyType list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $studyType = StudyType::with('study_type_results')->create($input);

        return $this->successResponse($studyType,'StudyType saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StudyType  $studyType
     * @return \Illuminate\Http\Response
     */
    public function show(StudyType $studyType)
    {

        $studyType_show = StudyType::with('study_type_results')->find($studyType->id);

        if (empty($studyType_show)) {
            return $this->errorResponse('StudyType not found',404);
        }
        return $this->successResponse($studyType_show,'StudyType show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StudyType  $studyType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudyType $studyType)
    {
        $studyType_update = StudyType::with('study_type_results')->find($studyType->id);

        if (empty($studyType_update)) {
            return $this->errorResponse('StudyType not found',404);
        }
        $studyType_update->fill($request->all());
        $studyType_update->save();

        return $this->successResponse($studyType_update,'StudyType updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StudyType  $studyType
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudyType $studyType)
    {
        $studyType_delete = StudyType::find($studyType->id);
        if(isset($studyType_delete->patients) && count($studyType_delete->patients))
        abort(500, "actives_relations");
        if (empty($studyType_delete)) {
            return $this->errorResponse('StudyType not found',404);
        }
        $studyType_delete->delete();
        return $this->successResponse($studyType_delete,'StudyType deleted', 200);
    }
}
