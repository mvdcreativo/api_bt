<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Sample;
use App\Models\SampleData;
use App\Models\SampleDataAnatomo;
use App\Models\StorageSample;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class SampleController extends Controller
{

    use ApiResponser;
    public function __construct()
    {
        $this->middleware(['permission:sample.index'])->only('index');
        $this->middleware(['permission:sample.show'])->only('show');
        $this->middleware(['permission:sample.update'])->only('update');
        $this->middleware(['permission:sample.delete'])->only('destroy');
        $this->middleware(['permission:sample.create'])->only('store');
    }


    public function index(Request $request)
    {
        $query = Sample::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        if ($request->get('patient_id')) {
            $patient_id = $request->get('patient_id');
        }else{
            $patient_id = "";
        }

        $samples = $query
        ->with('sample_data', 'patient', 'type_sample', 'tumor_lineage', 'topography', 'sample_data_anatomo', 'traces_sample', 'traces_sample_last' )
        ->filter($filter)
        ->patient_id($patient_id)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($samples,'Sample list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         try {
            $sample = new Sample;
            $sample->fill($request->all());
            $sample->save();

            if($sample){
                $sample_data = new SampleData;
                $request->get('sample_data') ? $sample_data->fill($request->get('sample_data')): false;
                $sample_data->sample()->associate($sample);
                $sample_data->save();
            }
            if($sample){
                $sample_data_anatomo = new SampleDataAnatomo();
                $request->get('sample_data_anatomo') ? $sample_data_anatomo->fill($request->get('sample_data_anatomo')) : false;
                $sample_data_anatomo->sample()->associate($sample);
                $sample_data_anatomo->save();
            }
        } catch (\Throwable $th) {
            return $th ;
        }



        $sample->sample_data;
        $sample->patient;
        $sample->type_sample;
        $sample->tumor_lineage;
        $sample->topography;
        $sample->sample_data_anatomo;

        // $sample->stages;

        return $this->successResponse($sample,'Sample saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sample  $samples
     * @return \Illuminate\Http\Response
     */
    public function show(Sample $sample)
    {

        $sample_show = Sample::with('sample_data', 'sample_data_anatomo', 'patient', 'type_sample', 'tumor_lineage', 'topography','storage_sample', 'traces_sample')->find($sample->id);

        if (empty($sample_show)) {
            return $this->errorResponse('Sample not found',404);
        }

        return $this->successResponse($sample_show,'Sample show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sample  $samples
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sample $sample)
    {
        // return $request->all();
        try {
            $samples_update = Sample::find($sample->id);
            $samples_update->fill($request->all());
            if($request->get('position')){
                $position = preg_split("/[L,R,B,P]/", $request->get('position'), 0, PREG_SPLIT_NO_EMPTY);
                $storage_sample = StorageSample::where('sample_id', $sample->id)->first();
                if(empty($storage_sample)){
                    StorageSample::create([
                        'code'=>$request->get('position'),
                        'sample_id'=>$sample->id,
                        'location_id'=>$position[0],
                        'rack'=>$position[1],
                        'box'=>$position[2],
                        'position'=>$position[3]
                    ]);
                }else{
                    $storage_sample->update([
                        'code'=>$request->get('position'),
                        'sample_id'=>$sample->id,
                        'location_id'=>$position[0],
                        'rack'=>$position[1],
                        'box'=>$position[2],
                        'position'=>$position[3]
                    ]);
                }
            }

            $data = $request->sample_data;
            $data_anatomo =   $request->sample_data_anatomo;
            if($samples_update && $request->sample_data){
                if($data['sample_id']){
                    $sample_data= SampleData::where('sample_id', $data['sample_id'])->firstOrFail();
                    $sample_data->fill($request->sample_data);
                }else{
                    $sample_data = new SampleData;
                    $sample_data->fill($request->sample_data);
                    $sample_data->sample()->associate($samples_update);
                }
                $sample_data->save();
            }

            if($samples_update && $request->sample_data_anatomo){
                if($data_anatomo['sample_id']){
                    $sample_data_anatomo= SampleDataAnatomo::where('sample_id', $data_anatomo['sample_id'])->firstOrFail();
                    $sample_data_anatomo->fill($request->sample_data_anatomo);
                }else{
                    $sample_data_anatomo = new SampleDataAnatomo;
                    $sample_data_anatomo->fill($request->sample_data_anatomo);
                    $sample_data_anatomo->sample()->associate($samples_update);
                }
                $sample_data_anatomo->save();
            }
            $samples_update->save();
            $samples_update->sample_data;
            $samples_update->patient;
            $samples_update->type_samples;
            $samples_update->tumor_lineage;
            $samples_update->topography;
            $samples_update->sample_data_anatomo;

            $samples_update->storage_sample;

            return $this->successResponse($samples_update,'Sample updated', 200);
        } catch (\Throwable $th) {
            return $th ;
        }


        // $samples->stages;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sample  $samples
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sample $sample)
    {
        $sample_delete = Sample::find($sample->id);
        if (empty($sample_delete)) {
            return $this->errorResponse('Sample not found',404);
        }

        // if(isset($sample_delete->storage_sample) && count($sample_delete->storage_sample))
        // abort(500, "actives_relations");

        $sample_delete->delete();
        return $this->successResponse($sample_delete,'Sample deleted', 200);
    }


    public function last_id_sample()
    {
        $id = Sample::latest('id')->first();

        return $this->successResponse($id,'Sample last id', 200);
    }

    public function code_sample_exist(Request $request)
    {

        if($request->not_include_id){
            $not_include_id = $request->not_include_id;
        }else{
            $not_include_id = null;
        }
        $code = $request->get('code');
        $exist = Sample::where('id','!=', $not_include_id)->where('code',$code)->first();

        if(empty($exist)){
            return response()->json(['exist'=>null], 200);
        }

        return response()->json(['exist'=>true], 200);
    }

    public function genCodeSample()
    {
        $last = Sample::latest('id')->first();
        if(empty($last)){
            $code = Sample::genCode( 1, 6 );
        }else{
            $code = Sample::genCode($last->id + 1, 6 );
        }
        return $this->successResponse($code,'Code available', 200);
    }


    public function clone_sample($id)
    {
        $last = Sample::latest('id')->first();

        $sample = Sample::find($id);
        $code = Sample::genCode($last->id + 1, 6 );

        if($code){
            $sample->code = $code;
            $clon_sample = $sample->duplicate();
            return $this->successResponse($clon_sample,'Sample Cloned', 200);
        }

    }
}
