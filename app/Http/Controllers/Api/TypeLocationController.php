<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TypeLocation;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class TypeLocationController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = TypeLocation::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        $type_locations = $query
        // ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($type_locations,'TypeLocation list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $type_location = TypeLocation::create($input);

        return $this->successResponse($type_location,'TypeLocation saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypeLocation  $type_location
     * @return \Illuminate\Http\Response
     */
    public function show(TypeLocation $type_location)
    {

        $type_location_show = TypeLocation::find($type_location->id);

        if (empty($type_location_show)) {
            return $this->errorResponse('TypeLocation not found',404);
        }
        return $this->successResponse($type_location_show,'TypeLocation show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TypeLocation  $type_location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TypeLocation $type_location)
    {
        $type_location_update = TypeLocation::find($type_location->id);

        if (empty($type_location_update)) {
            return $this->errorResponse('TypeLocation not found',404);
        }
        $type_location_update->fill($request->all());
        $type_location_update->save();

        return $this->successResponse($type_location_update,'TypeLocation updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeLocation  $type_location
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypeLocation $type_location)
    {
        $type_location_delete = TypeLocation::find($type_location->id);

        if(isset($type_location_delete->locations) && count($type_location_delete->locations))
        abort(500, "actives_relations");

        if (empty($type_location_delete)) {
            return $this->errorResponse('TypeLocation not found',404);
        }
        $type_location_delete->delete();
        return $this->successResponse($type_location_delete,'TypeLocation deleted', 200);
    }
}
