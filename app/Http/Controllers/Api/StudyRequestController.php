<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\StudyRequest;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class StudyRequestController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        $this->middleware(['permission:study_request.index'])->only('index');
        $this->middleware(['permission:study_request.show'])->only('show');
        $this->middleware(['permission:study_request.update'])->only('update');
        $this->middleware(['permission:study_request.delete'])->only('destroy');
        $this->middleware(['permission:study_request.create'])->only('store');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = StudyRequest::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        if ($request->get('patient_id')) {
            $patient_id = $request->get('patient_id');
        }else{
            $patient_id = "";
        }

        if ($request->get('sample_id')) {
            $saple_id = $request->get('sample_id');
        }else{
            $saple_id = "";
        }

        $studyRequests = $query
        ->with('user','financing','lab','sample','study_type', 'sample', 'patient', 'result')
        ->filter($filter)
        ->sampleId($saple_id)
        ->patientId($patient_id)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($studyRequests,'StudyRequest list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();

        $studyRequest = StudyRequest::create($input);
        $studyRequest = StudyRequest::with('user','financing','lab','sample','study_type', 'sample', 'patient', 'result')->find($studyRequest->id);


        return $this->successResponse($studyRequest,'StudyRequest saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StudyRequest  $studyRequest
     * @return \Illuminate\Http\Response
     */
    public function show(StudyRequest $studyRequest)
    {

        $result_show = StudyRequest::with('user','financing','lab','sample','study_type', 'sample', 'patient', 'result')->find($studyRequest->id);

        if (empty($result_show)) {
            return $this->errorResponse('StudyRequest not found',404);
        }

        return $this->successResponse($result_show,'StudyRequest show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StudyRequest  $studyRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudyRequest $studyRequest)
    {
        $result_update = StudyRequest::find($studyRequest->id);

        if (empty($result_update)) {
            return $this->errorResponse('StudyRequest not found',404);
        }


        $result_update->fill($request->all());
        $result_update->save();
        $result_update = StudyRequest::with('user','financing','lab','sample','study_type', 'sample', 'patient', 'result')->find($studyRequest->id);

        return $this->successResponse($result_update,'StudyRequest updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StudyRequest  $studyRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudyRequest $studyRequest)
    {
        $result_delete = StudyRequest::find($studyRequest->id);
        if(isset($result_delete->patients) && count($result_delete->patients))
        abort(500, "actives_relations");
        if (empty($result_delete)) {
            return $this->errorResponse('StudyRequest not found',404);
        }
        $result_delete->delete();
        return $this->successResponse($result_delete,'StudyRequest deleted', 200);
    }
}
