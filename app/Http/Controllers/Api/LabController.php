<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Lab;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class LabController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Lab::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        $labs = $query
        ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($labs,'Lab list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $lab = Lab::create($input);

        return $this->successResponse($lab,'Lab saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lab  $lab
     * @return \Illuminate\Http\Response
     */
    public function show(Lab $lab)
    {

        $lab_show = Lab::find($lab->id);

        if (empty($lab_show)) {
            return $this->errorResponse('Lab not found',404);
        }
        return $this->successResponse($lab_show,'Lab show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lab  $lab
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lab $lab)
    {
        $lab_update = Lab::find($lab->id);

        if (empty($lab_update)) {
            return $this->errorResponse('Lab not found',404);
        }
        $lab_update->fill($request->all());
        $lab_update->save();

        return $this->successResponse($lab_update,'Lab updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lab  $lab
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lab $lab)
    {
        $lab_delete = Lab::find($lab->id);
        if(isset($lab_delete->patients) && count($lab_delete->patients))
        abort(500, "actives_relations");
        if (empty($lab_delete)) {
            return $this->errorResponse('Lab not found',404);
        }
        $lab_delete->delete();
        return $this->successResponse($lab_delete,'Lab deleted', 200);
    }
}
