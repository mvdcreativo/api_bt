<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Tnm;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class TnmController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Tnm::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        $tnms = $query
        ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($tnms,'Tnm list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $tnm = Tnm::create($input);

        return $this->successResponse($tnm,'Tnm saved', 201);
    }

     /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tnm  $tnm
     * @return \Illuminate\Http\Response
     */
    public function show(Tnm $tnm)
    {

        $tnm_show = Tnm::find($tnm->id);

        if (empty($tnm_show)) {
            return $this->errorResponse('Tnm not found',404);
        }
        return $this->successResponse($tnm_show,'Tnm show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tnm  $tnm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tnm $tnm)
    {
        $tnm_update = Tnm::find($tnm->id);

        if (empty($tnm_update)) {
            return $this->errorResponse('Tnm not found',404);
        }
        $tnm_update->fill($request->all());
        $tnm_update->save();

        return $this->successResponse($tnm_update,'Tnm updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tnm  $tnm
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

            $tnm_delete = Tnm::find($id);

            if(isset($tnm_delete->samples) && count($tnm_delete->samples))
                abort(500, "actives_relations");

            if (empty($tnm_delete)) {
                return $this->errorResponse('Tnm not found',404);
            }

            $tnm_delete->delete();

            return $this->successResponse($tnm_delete,'Tnm deleted', 200);

    }
}
