<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CancerPatientAntecedent;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class CancerPatientAntecedentController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $cancerPatientAntecedent = CancerPatientAntecedent::create($input);
        $cancerPatientAntecedent->topography;

        return $this->successResponse($cancerPatientAntecedent,'CancerPatientAntecedent saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CancerPatientAntecedent  $cancerPatientAntecedent
     * @return \Illuminate\Http\Response
     */
    public function show(CancerPatientAntecedent $cancerPatientAntecedent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CancerPatientAntecedent  $cancerPatientAntecedent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CancerPatientAntecedent $cancerPatientAntecedent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CancerPatientAntecedent  $cancerPatientAntecedent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cancerPatientAntecedent_delete = CancerPatientAntecedent::find($id);

        if (empty($cancerPatientAntecedent_delete)) {
            return $this->errorResponse('CancerPatientAntecedent not found',404);
        }
        $cancerPatientAntecedent_delete->delete();
        return $this->successResponse($cancerPatientAntecedent_delete,'CancerPatientAntecedent deleted', 200);
    }
}
