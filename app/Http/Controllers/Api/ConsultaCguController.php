<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ConsultaCgu;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class ConsultaCguController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = ConsultaCgu::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        $consulta_cgus = $query
        ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($consulta_cgus,'ConsultaCgu list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $consulta_cgu = ConsultaCgu::create($input);

        return $this->successResponse($consulta_cgu,'ConsultaCgu saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ConsultaCgu  $consulta_cgu
     * @return \Illuminate\Http\Response
     */
    public function show(ConsultaCgu $consulta_cgu)
    {

        $consulta_cgu_show = ConsultaCgu::find($consulta_cgu->id);

        if (empty($consulta_cgu_show)) {
            return $this->errorResponse('ConsultaCgu not found',404);
        }
        return $this->successResponse($consulta_cgu_show,'ConsultaCgu show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ConsultaCgu  $consulta_cgu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConsultaCgu $consulta_cgu)
    {
        $consulta_cgu_update = ConsultaCgu::find($consulta_cgu->id);

        if (empty($consulta_cgu_update)) {
            return $this->errorResponse('ConsultaCgu not found',404);
        }
        $consulta_cgu_update->fill($request->all());
        $consulta_cgu_update->save();

        return $this->successResponse($consulta_cgu_update,'ConsultaCgu updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ConsultaCgu  $consulta_cgu
     * @return \Illuminate\Http\Response
     */
    public function destroy(ConsultaCgu $consulta_cgu)
    {
        $consulta_cgu_delete = ConsultaCgu::find($consulta_cgu->id);
        if(isset($consulta_cgu_delete->patients) && count($consulta_cgu_delete->patients))
        abort(500, "actives_relations");
        if (empty($consulta_cgu_delete)) {
            return $this->errorResponse('ConsultaCgu not found',404);
        }
        $consulta_cgu_delete->delete();
        return $this->successResponse($consulta_cgu_delete,'ConsultaCgu deleted', 200);
    }
}
