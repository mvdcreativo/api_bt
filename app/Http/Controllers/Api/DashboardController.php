<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Sample;
use App\Models\TraceSample;
use App\Traits\ApiResponser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    use ApiResponser;

    public function index(Request $request)
    {

        // request (model, colum, filter_value, conditional )
        $model = ucfirst($request->get('model'));
        $model_relation = $request->get('model_relation');
        $column = $request->get('column');
        $filter_value = $request->get('filter_value');
        $conditional = $request->get('conditional');


        $response = 0;
        try {
            $r = new \ReflectionClass("App\\Models\\" . $model);
            $instance = $r->newInstanceWithoutConstructor();

            //devuelve Cantidad de registros, pasandole modelo, columna a comparar, valor a comparar y condicional (=, != etc)
            if (isset($model) && !isset($model_relation) && isset($column) && isset($filter_value) && isset($conditional)) {
                $response = $instance->where($column, $conditional, $filter_value)->count();
            }
            //devuelve Cantidad de registros, pasandole modelo, metodo de relacion, columna a comparar, valor a comparar y condicional (=, != etc)
            if (isset($model) && isset($model_relation) && isset($column) && isset($filter_value) && isset($conditional)) {

                $response = $instance->query()->whereHas($model_relation, function(Builder $q) use ($filter_value, $column, $conditional){
                    $q->where($column, $conditional, $filter_value);
                })->count();
            }

            //devuelve Cantidad de registros ingresados para ese modelo.
            if (isset($model) && !isset($model_relation) && !isset($column) && !isset($filter_value) && !isset($conditional)) {
                $response = $instance->count();
            }




            return $this->successResponse($response, '', 200);

        } catch (\Throwable $th) {

            return $th;
            // return $this->errorResponse($th, 405);
        }
    }


    public function samples_end(Request $request)
    {



        $response = 0;
        try {
            $samples = Sample::with('traces_sample')->get();

            $stages = $samples->filter(function ($sample, $key) {

                $trace = $sample->traces_sample->last();

                if ($trace && $trace->stage->end == 1) {
                   return true;
                }
                return false;
            })->count();


            return $this->successResponse($stages, '', 200);

        } catch (\Throwable $th) {

            return $th;
            // return $this->errorResponse($th, 405);
        }
    }

    public function samples_in_process(Request $request)
    {

        try {
            $samples = Sample::with('traces_sample')->get();

            $stages = $samples->filter(function ($sample, $key) {

                $trace = $sample->traces_sample->last();

                if ($trace && !$trace->stage->end == 1) {
                   return true;
                }
                return false;
            })->count();


            return $this->successResponse($stages, '', 200);

        } catch (\Throwable $th) {

            return $th;
            // return $this->errorResponse($th, 405);
        }
    }
}
