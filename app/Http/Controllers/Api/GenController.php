<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Gen;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class GenController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Gen::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        $gens = $query
        ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($gens,'Gen list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $gen = Gen::create($input);

        return $this->successResponse($gen,'Gen saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gen  $gen
     * @return \Illuminate\Http\Response
     */
    public function show(Gen $gen)
    {

        $gen_show = Gen::find($gen->id);

        if (empty($gen_show)) {
            return $this->errorResponse('Gen not found',404);
        }
        return $this->successResponse($gen_show,'Gen show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gen  $gen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gen $gen)
    {
        $gen_update = Gen::find($gen->id);

        if (empty($gen_update)) {
            return $this->errorResponse('Gen not found',404);
        }
        $gen_update->fill($request->all());
        $gen_update->save();

        return $this->successResponse($gen_update,'Gen updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gen  $gen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gen $gen)
    {
        $gen_delete = Gen::find($gen->id);
        if(isset($gen_delete->patients) && count($gen_delete->patients))
        abort(500, "actives_relations");
        if (empty($gen_delete)) {
            return $this->errorResponse('Gen not found',404);
        }
        $gen_delete->delete();
        return $this->successResponse($gen_delete,'Gen deleted', 200);
    }
}
