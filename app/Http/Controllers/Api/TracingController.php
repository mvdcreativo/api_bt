<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Tracing;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class TracingController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Tracing::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }

        if ($request->get('filter')) {
            $filter = $request->get('filter');
        }else{
            $filter = "";
        }

        $tracings = $query
        ->filter($filter)
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($tracings,'Tracing list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $tracing = Tracing::create($input);

        return $this->successResponse($tracing,'Tracing saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tracing  $tracing
     * @return \Illuminate\Http\Response
     */
    public function show($patient_id)
    {

        $tracing_show = Tracing::where('patient_id', $patient_id)->first();


        return $this->successResponse($tracing_show,'Tracing show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tracing  $tracing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tracing $tracing)
    {
        $tracing_update = Tracing::find($tracing->id);

        if (empty($tracing_update)) {
            return $this->errorResponse('Tracing not found',404);
        }
        $tracing_update->fill($request->all());
        $tracing_update->save();

        return $this->successResponse($tracing_update,'Tracing updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tracing  $tracing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tracing $tracing)
    {
        $tracing_delete = Tracing::find($tracing->id);
        // if(isset($tracing_delete->sample_data_anatomos) && count($tracing_delete->sample_data_anatomos))
        // abort(500, "actives_relations");

        if (empty($tracing_delete)) {
            return $this->errorResponse('Tracing not found',404);
        }
        $tracing_delete->delete();
        return $this->successResponse($tracing_delete,'Tracing deleted', 200);
    }
}
