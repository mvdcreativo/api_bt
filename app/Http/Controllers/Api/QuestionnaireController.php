<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Patient;
use App\Models\QuestionaryPatient;
use App\Models\Questionnaire;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class QuestionnaireController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Questionnaire::query();

        if ($request->get('per_page')) {
            $per_page = $request->get('per_page');
        }else{
            $per_page = 20;
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }else{
            $sort = "desc";
        }


        $questionnaires = $query
        ->orderBy('id', $sort)
        ->paginate($per_page);

        return $this->successResponse($questionnaires,'Questionnaire list', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $questionnaire = Questionnaire::create($input);

        return $this->successResponse($questionnaire,'Questionnaire saved', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function show(Questionnaire $questionnaire)
    {

        $questionnaire_show = Questionnaire::find($questionnaire->id);

        if (empty($questionnaire_show)) {
            return $this->errorResponse('Questionnaire not found',404);
        }
        return $this->successResponse($questionnaire_show,'Questionnaire show', 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Questionnaire $questionnaire)
    {
        $questionnaire_update = Questionnaire::find($questionnaire->id);

        if (empty($questionnaire_update)) {
            return $this->errorResponse('Questionnaire not found',404);
        }
        $questionnaire_update->fill($request->all());
        $questionnaire_update->save();

        return $this->successResponse($questionnaire_update,'Questionnaire updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questionnaire $questionnaire)
    {
        $questionnaire_delete = Questionnaire::find($questionnaire->id);
        if(isset($questionnaire_delete->sample_data_anatomos) && count($questionnaire_delete->sample_data_anatomos))
        abort(500, "actives_relations");

        if (empty($questionnaire_delete)) {
            return $this->errorResponse('Questionnaire not found',404);
        }
        $questionnaire_delete->delete();
        return $this->successResponse($questionnaire_delete,'Questionnaire deleted', 200);
    }

    public function addPatient(Request $request)
    {

        $patient_id= $request->get('patient_id');

        // return $input;
        $relation = QuestionaryPatient::create($request->all());
        if($relation){
            $patient = Patient::find($patient_id);
        }

        // $newItem = BdInternat ionalPatient::create($input);
        return $this->successResponse($patient->questionnaires,'Created Patient Questionary relationship', 201);

    }

    public function removePatient($id, Request $request)
    {
        $relation = QuestionaryPatient::find($id);

        if (empty($relation)) {
            return $this->errorResponse('Patient Questionary relationship not found',404);
        }
        $relation->delete();

        if($relation){
            $patient = Patient::find($request->get('patient_id'));
            return $this->successResponse($patient->questionnaires,'relationship deleted', 200);

        }
    }

}
