<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CancerPatientAntecedent extends Model
{
    use HasFactory;

    protected $fillable = [
        'patient_id',
        'topography_id',
        'edad'
    ];

    ///RELATIONSHIP
    public function patient()
    {
        return $this->belongsTo('App\Models\Patient');
    }
    public function topography()
    {
        return $this->belongsTo('App\Models\Topography');
    }
}
