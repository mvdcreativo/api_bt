<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Topography extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'cie10'
    ];


    public function samples()
    {
        return $this->hasMany('App\Models\Sample');
    }

    public function tracings()
    {
        return $this->hasMany(Tracing::class);
    }

    public function cancer_patient_antecedent()
    {
        return $this->hasMany('App\Models\CancerPatientAntecedent');
    }






    /////////////////////////////
        ///SCOPES
    /////////////////////////////

    public function scopeFilter($query, $filter)
    {
        if($filter)
            return $query
            ->orWhere('name', "LIKE", '%'.$filter.'%')
            ->orWhere('cie10', "LIKE", '%'.$filter.'%');
            // ->orWhere('id', "LIKE", '%'.$filter.'%');
    }
}
