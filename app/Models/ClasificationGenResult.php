<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Relations\Pivot;

class ClasificationGenResult extends Pivot
{


    protected $table = 'gens_results';

    protected $with=['gen', 'clasification'];


    public function gen()
    {
        return $this->belongsTo(Gen::class);
    }

    public function clasification()
    {
        return $this->belongsTo(Clasification::class);

    }




}
