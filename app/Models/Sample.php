<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Self_;

class Sample extends Model
{
    use HasFactory;
    ///Para clonar
    use \Bkwld\Cloner\Cloneable;

    protected $fillable = [
        'code',
        'patient_id',
        'type_sample_id',
        'tumor_lineage_id',
        'topography_id',
        'position',
        'last_stage_status',

    ];


    public function patient()
    {
        return $this->belongsTo('App\Models\Patient');
    }

    public function type_sample()
    {
        return $this->belongsTo('App\Models\TypeSample');
    }

    public function tumor_lineage()
    {
        return $this->belongsTo('App\Models\TumorLineage');
    }

    public function topography()
    {
        return $this->belongsTo('App\Models\Topography');
    }

    public function sample_data()
    {
        return $this->hasOne('App\Models\SampleData');
    }

    public function sample_data_anatomo()
    {
        return $this->hasOne('App\Models\SampleDataAnatomo');
    }

    public function traces_sample()
    {
        return $this->hasMany(TraceSample::class)->with('stage')->orderBy('id','desc');
    }
    public function traces_sample_last()
    {
        return $this->hasOne(TraceSample::class)->orderBy('id','desc')->with('stage')->latest();
    }


    public function storage_sample()
    {
        return $this->hasOne(StorageSample::class)->with('location');
    }



    /////////////////////////////
    ///SCOPES
    /////////////////////////////

    public function scopeFilter($query, $filter)
    {
        if ($filter)
            return $query
                ->where('code', "LIKE", '%'.$filter.'%')
                ->orWhereHas('type_sample', function(Builder $q) use ($filter){
                    $q->where('name', "LIKE", '%'.$filter.'%');
                })
                ->orWhereHas('topography', function(Builder $q) use ($filter){
                    $q->where('name', "LIKE", '%'.$filter.'%');
                })
                ->orWhereHas('patient', function(Builder $q) use ($filter){
                    $q->where('code', "LIKE", '%'.$filter.'%');
                    $q->orWhere('name', "LIKE", '%'.$filter.'%');
                    $q->orWhere('last_name', "LIKE", '%'.$filter.'%');
                    $q->orWhere('n_doc', "LIKE", '%'.$filter.'%');
                })

                ->orWhereHas('traces_sample_last', function(Builder $q) use ($filter){
                    $q->filterNowStage($filter);
                })
                ;
    }

    public function scopePatient_id($query, $filter)
    {
        if ($filter)
            return $query
                ->where('patient_id', $filter);
    }


    ///Para clonar
    protected $cloneable_relations = ['sample_data', 'sample_data_anatomo'];
    protected $clone_exempt_attributes = ['position'];

    public function onCloning()
    {


    }

    public static function genCode($num, $padlen, $padchar = null) {
        $padchar = $padchar !== null ? $padchar : '0';
        $pad = implode(array_pad([], -$padlen, $padchar));
        $code = "#HM" . substr($pad . $num, -strlen($pad));

        $code_exist = function($code){
            return Self::where('code', $code)->first() ? true : false;
        };
        $check = $code_exist($code);
        if($check){
            return Self::genCode($num+1, 6);
        }else{
            return $code;
        }

    }
    /////////////
}
