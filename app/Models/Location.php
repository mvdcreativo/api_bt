<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;


    protected $fillable = ['type_location_id','code','racks','boxes','positions','description','brand_model','capacity'];

    public function type_location()
    {
        return $this->belongsTo('App\Models\TypeLocation');
    }

    public function storage_sample()
    {
        return $this->hasMany(StorageSample::class);
    }
}
