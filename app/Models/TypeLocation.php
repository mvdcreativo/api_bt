<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeLocation extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
    ];

    public function locations()
    {
        return $this->hasMany('App\Models\Location');
    }
}
