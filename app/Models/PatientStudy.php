<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;


class PatientStudy extends Pivot
{
    use HasFactory;
    protected $table = "studies_patients";
    protected $fillable =[
        'id',
        'patient_id',
        'study_id',
        'date',
        'obs',
    ];



    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    public function study()
    {
        return $this->belongsTo(InternationalBd::class);
    }
}
