<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientScore extends Model
{
    use HasFactory;

    protected $fillable =[
        'patient_id',
        'score',
        'percentage',
    ];


    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

}
