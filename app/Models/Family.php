<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'dg_id',
        'studied',
        'consulta_cgu_id'
    ];

    public function patients()
    {
        return $this->belongsToMany('App\Models\Patient', 'family_kinship_patient')
        ->using(FamilyKinshipPatient::class)
        ->withPivot('kinship_id','patient_id');
    }

    public function kinships()
    {
        return $this->belongsToMany('App\Models\Kinship', 'family_kinship_patient')->using(FamilyKinshipPatient::class)->withPivot('patient_id');

    }

    public function dg()
    {
        return $this->belongsTo(Dg::class);
    }

    public function consulta_cgus()
    {
        return $this->belongsTo(ConsultaCgu::class);
    }


    /////////////////////////////
        ///SCOPES
    /////////////////////////////


    public function scopeFilter($query, $filter)
    {
        if($filter)
            return $query
            ->where('code', "LIKE", '%'.$filter.'%');
    }

    public function scopePatient_id($query, $filter)
    {
        if($filter){
            $query
            ->whereHas('patients', function(Builder $q) use ($filter){
                $q->where('patient_id', $filter);
            });
            return $query;
        }
    }


}
