<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;


class BdInternationalPatient extends Pivot
{
    use HasFactory;
    protected $table = "international_bds_patients";
    protected $fillable =[
        'id',
        'patient_id',
        'international_bd_id',
        'date',
        'obs',
    ];



    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    public function bd_international()
    {
        return $this->belongsTo(InternationalBd::class);
    }
}
