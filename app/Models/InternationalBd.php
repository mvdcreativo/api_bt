<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InternationalBd extends Model
{
    use HasFactory;
    protected $fillable =[
        'name'
    ];


    public function patients()
    {
        return $this->belongsToMany(Patient::class, 'international_bds_patients');
    }
}
