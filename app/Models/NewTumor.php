<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewTumor extends Model
{
    use HasFactory;
    protected $fillable =[
        'patient_id',
        'topography_id',
        'date',
        'obs',
    ];



    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    public function topography()
    {
        return $this->belongsTo(Topography::class);
    }
}
