<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tracing extends Model
{
    use HasFactory;

    protected $fillable =[
        'patient_id',

        'mas_uni',
        'mas_uni_date',
        'mas_uni_obs',
        'mas_bi',
        'mas_bi_date',
        'mas_bi_obs',
        'oof',
        'oof_date',
        'oof_obs',
        'hist',
        'hist_date',
        'hist_obs',
        'gast',
        'gast_date',
        'gast_obs',
        'qui',
        'qui_date',
        'qui_obs',
        'onc',
        'onc_date',
        'onc_obs',
    ];


    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }



}
