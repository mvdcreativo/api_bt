<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Relations\Pivot;

class FamilyKinshipPatient extends Pivot
{


    protected $table = 'family_kinship_patient';

    protected $with=['kinship', 'patient' , 'family'];


    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    public function kinship()
    {
        return $this->belongsTo(Kinship::class);

    }

    public function family()
    {
        return $this->belongsTo(Family::class);

    }




}
