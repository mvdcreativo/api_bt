<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;


class QuestionaryPatient
 extends Pivot
{
    use HasFactory;
    protected $table = "questionnaires_patients";
    protected $fillable =[
        'id',
        'patient_id',
        'questionnaire_id',
        'date',
        'obs',
    ];



    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    public function study()
    {
        return $this->belongsTo(Questionnaire::class);
    }
}
