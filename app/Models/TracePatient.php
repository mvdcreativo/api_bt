<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TracePatient extends Model
{
    use HasFactory;

    protected $fillable = [
        'patient_id',
        'evolution_id',
        'user_name',
        'obs',
        'date_limit',
        'user_id'
    ];

    public function evolution()
    {
        return $this->belongsTo(Evolution::class);
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }
        /////////////////////////////
        ///SCOPES
    /////////////////////////////

    public function scopeFilter($query, $filter)
    {
        if($filter)
            return $query
            ->where('user_name', "LIKE", '%'.$filter.'%')
            ->orWhere('created_at', "LIKE", '%'.$filter.'%')
            ->orWhere('date_limit', "LIKE", '%'.$filter.'%')
            ->orWhere('obs', "LIKE", '%'.$filter.'%');

    }

    public function scopePatient_id($query, $filter)
    {
        if($filter)
            return $query
            ->where('patient_id', $filter);
    }
}
