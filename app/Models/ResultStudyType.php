<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResultStudyType extends Model
{
    use HasFactory;

    protected $fillable = [
        'study_type_id',
        'name',
    ];

    /////////////////////////////
        ///SCOPES
    /////////////////////////////

    public function scopeFilter($query, $filter)
    {
        if($filter)
            return $query
            ->where('name', "LIKE", '%'.$filter.'%')
            ->orWhere('id', "LIKE", '%'.$filter.'%');
    }
}
