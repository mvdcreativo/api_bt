<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConsultaCgu extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'last_name',
        'contact'
    ];

    public function families()
    {
        return $this->hasMany('App\Models\Family');
    }


    /////////////////////////////
        ///SCOPES
    /////////////////////////////

    public function scopeFilter($query, $filter)
    {
        if($filter)
            return $query
            ->where('name', "LIKE", '%'.$filter.'%')
            ->orWhere('id', "LIKE", '%'.$filter.'%')
            ->orWhere('contact', "LIKE", '%'.$filter.'%');
    }
}
