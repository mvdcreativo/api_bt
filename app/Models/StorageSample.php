<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StorageSample extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'sample_id',
        'location_id',
        'rack',
        'box',
        'position'
    ];



    public function sample()
    {
        return $this->belongsTo(Sample::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }



    public function scopePatient_id($query, $filter)
    {
        if($filter)
            return $query
            ->whereHas('sample', function(Builder $q) use ($filter){
                $q->where('patient_id', $filter);
            });
    }
}
