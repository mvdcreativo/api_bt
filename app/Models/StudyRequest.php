<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class StudyRequest extends Model
{
    use HasFactory;
    protected $fillable =[
        'user_id',
        'date_sol',
        'study_type_id',
        'lab_id',
        'financing_id',
        'patient_id',
        'status_req',
        'sample_id',
    ];


    /*
    Relationships
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function result()
    {
        return $this->hasOne(Result::class);
    }
    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }
    public function sample()
    {
        return $this->belongsTo(Sample::class);
    }
    public function financing()
    {
        return $this->belongsTo(Financing::class);
    }
    public function lab()
    {
        return $this->belongsTo(Lab::class);
    }

    public function study_type()
    {
        return $this->belongsTo(StudyType::class)->with('study_type_results');
    }



    /////////////////////////////
        ///SCOPES
    /////////////////////////////

    public function scopeFilter($query, $filter)
    {
        if($filter) {
            return $query
            ->where('date_sol', "LIKE", '%'.$filter.'%')
            ->orWhere('status_req', "LIKE", '%'.$filter.'%')
            ->orWhereHas('sample', function(Builder $q) use ($filter){
                $q->where('code', "LIKE", '%'.trim($filter).'%');
            })
            ->orWhereHas('patient', function(Builder $q) use ($filter){
                $q->where('name', "LIKE", '%'.trim($filter).'%');
                $q->orWhere('last_name', "LIKE", '%'.trim($filter).'%');
                $q->orWhere('phone', "LIKE", '%'.trim($filter).'%');
                $q->orWhere('email', "LIKE", '%'.trim($filter).'%');
                $q->orWhere('n_doc', "LIKE", '%'.trim($filter).'%');
                $q->orWhere('code', "LIKE", '%'.trim($filter).'%');
            })
            ->orWhereHas('study_type', function(Builder $q) use ($filter){
                $q->where('name', "LIKE", '%'.trim($filter).'%');
            });
        }
    }

    public function scopePatientId($query, $patient_id){
        if($patient_id)
            return $query->where('patient_id',$patient_id);
    }

    public function scopeSampleId($query, $sample_id){
        if($sample_id)
            return $query->where('sample_id',$sample_id);
    }
}
