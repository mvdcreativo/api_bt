<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TraceSample extends Model
{
    use HasFactory;
    protected $fillable = [
        'sample_id',
        'stage_id',
        'user_name',
        'obs',
        'date_limit',
        'user_id'
    ];



    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    public function sample()
    {
        return $this->belongsTo(Sample::class);
    }

    /////////////////////////////
        ///SCOPES
    /////////////////////////////

    public function scopeFilter($query, $filter)
    {
        if($filter)
            return $query
            ->where('user_name', "LIKE", '%'.$filter.'%')
            ->orWhere('created_at', "LIKE", '%'.$filter.'%')
            ->orWhere('date_limit', "LIKE", '%'.$filter.'%')
            ->orWhere('obs', "LIKE", '%'.$filter.'%')
            ->orWhereHas('stage', function(Builder $q) use ($filter){
                $q->where('name', "LIKE", '%'.$filter.'%');
            });

    }

    public function scopeFilterNowStage($query, $filter)
    {
        if($filter)
            return $query
            ->whereHas('stage', function(Builder $q) use ($filter){
                $q->where('name', "LIKE", '%'.$filter.'%');
            });


    }

    public function scopeFilter_end($query)
    {

            return $query
            ->whereHas('stage', function(Builder $q){
                $q->where('end', 1);
            });

    }

    public function scopeSample_id($query, $filter)
    {
        if($filter)
            return $query
            ->where('sample_id', $filter);
    }
}
