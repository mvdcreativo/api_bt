<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    use HasFactory;
    protected $fillable = [
        'patient_id',
        'sample_id',
        'lab_id',
        'financing_id',
        'study_type_id',
        'result_study_type_id',
        'date_sol',
        'date_recep',
        'date_entrega',
        'obs_result',
        'reclasificado',
        'study_request_id',
        'reclas_obs'
    ];



    /*
    Relationships
    */
    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }
    public function sample()
    {
        return $this->belongsTo(Sample::class);
    }
    public function financing()
    {
        return $this->belongsTo(Financing::class);
    }
    public function lab()
    {
        return $this->belongsTo(Lab::class);
    }
    public function study_request()
    {
        return $this->belongsTo(StudyRequest::class);
    }

    public function study_type()
    {
        return $this->belongsTo(StudyType::class)->with('study_type_results');
    }
    public function result_study_type()
    {
        return $this->belongsTo(ResultStudyType::class);
    }


    public function gens_results()
    {
        return $this->belongsToMany(Gen::class, 'gens_results')->using(ClasificationGenResult::class)->withPivot('clasification_id','result_id','localization');
    }

    public function clasifications_results()
    {
        return $this->belongsToMany(Clasification::class, 'gens_results')->using(ClasificationGenResult::class)->withPivot('gen_id','result_id','localization');
    }



    /////////////////////////////
        ///SCOPES
    /////////////////////////////

    public function scopeFilter($query, $filter)
    {
        if($filter)
            return $query
            ->orWhere('date_sol', "LIKE", '%'.$filter.'%')
            ->orWhere('date_recep', "LIKE", '%'.$filter.'%')
            ->orWhere('date_entrega', "LIKE", '%'.$filter.'%')
            ->orWhere('reclasificado', "LIKE", '%'.$filter.'%')
            ->orWhere('obs_result', "LIKE", '%'.$filter.'%')
            ->orWhere('reclas_obs', "LIKE", '%'.$filter.'%')
            ->orWhereHas('sample', function(Builder $q) use ($filter){
                $q->where('code', "LIKE", '%'.trim($filter).'%');
            })
            ->orWhereHas('study_type', function(Builder $q) use ($filter){
                $q->where('name', "LIKE", '%'.trim($filter).'%');
            })
            ->orWhereHas('result_study_type', function(Builder $q) use ($filter){
                $q->where('name', "LIKE", '%'.trim($filter).'%');
            });
    }

    public function scopePatientId($query, $patient_id){
        if($patient_id)
            return $query->where('patient_id',$patient_id);
    }

    public function scopeSampleId($query, $sample_id){
        if($sample_id)
            return $query->where('sample_id',$sample_id);
    }

}
