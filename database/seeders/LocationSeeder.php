<?php

namespace Database\Seeders;

use App\Models\Location;
use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Location::create([
            'type_location_id'=> 1,
            'code' => 'F01',
            'racks' => 10,
            'boxes' => 8,
            'positions' => 46,
            'description' => 'Freezer-80',
            'brand_model' => 'Thermo F80',
            'capacity' => '800',
        ]);
        Location::create([
            'type_location_id'=> 1,
            'code' => 'F02',
            'racks' => 8,
            'boxes' => 8,
            'positions' => 46,
            'description' => 'Freezer-100',
            'brand_model' => 'Thermo F100',
            'capacity' => '1000',
        ]);
        Location::create([
            'type_location_id'=> 2,
            'code' => 'AT01',
            'racks' => 10,
            'boxes' => 1,
            'positions' => 90,
            'description' => 'Archivador Tacos',
            // 'brand_model' => '',
            // 'capacity' => '800',
        ]);
        Location::create([
            'type_location_id'=> 1,
            'code' => 'MY01',
            'racks' => 10,
            'boxes' => 1,
            'positions' => 90,
            'description' => 'Archivador Mucosa',
            // 'brand_model' => 'Thermo F80',
            // 'capacity' => '800',
        ]);

    }
}
