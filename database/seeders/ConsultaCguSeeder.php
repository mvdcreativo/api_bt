<?php

namespace Database\Seeders;

use App\Models\ConsultaCgu;
use Illuminate\Database\Seeder;

class ConsultaCguSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ConsultaCgu::factory(20)->create();
    }
}
