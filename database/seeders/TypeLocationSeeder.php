<?php

namespace Database\Seeders;

use App\Models\TypeLocation;
use Illuminate\Database\Seeder;

class TypeLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypeLocation::create([
            'name'=> 'Freezer'
        ]);
        TypeLocation::create([
            'name'=> 'Archivador Tacos'
        ]);
        TypeLocation::create([
            'name'=> 'Archivador MY'
        ]);
    }
}
