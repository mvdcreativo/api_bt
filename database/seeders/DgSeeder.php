<?php

namespace Database\Seeders;

use App\Models\Dg;
use Illuminate\Database\Seeder;

class DgSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Dg::create([
            'name' => 'dg 1'
        ]);

        Dg::create([
            'name' => 'dg 2'
        ]);

        Dg::create([
            'name' => 'dg 3'
        ]);
    }
}
