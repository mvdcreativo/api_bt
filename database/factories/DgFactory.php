<?php

namespace Database\Factories;

use App\Models\Dg;
use Illuminate\Database\Eloquent\Factories\Factory;

class DgFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Dg::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->firstName,

        ];
    }
}
