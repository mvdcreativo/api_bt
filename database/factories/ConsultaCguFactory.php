<?php

namespace Database\Factories;

use App\Models\ConsultaCgu;
use Illuminate\Database\Eloquent\Factories\Factory;

class ConsultaCguFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ConsultaCgu::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'contact' => "0".rand(91100501, 97999999),

        ];
    }
}
