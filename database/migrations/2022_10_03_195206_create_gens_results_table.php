<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGensResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gens_results', function (Blueprint $table) {
            $table->id();
            $table->string('localization');

            $table->unsignedBigInteger('result_id');
            $table->unsignedBigInteger('gen_id');
            $table->unsignedBigInteger('clasification_id')->nullable();

            $table->timestamps();

            $table->foreign('result_id')->references('id')->on('results')->onDelete('cascade');
            $table->foreign('gen_id')->references('id')->on('gens')->onDelete('cascade');
            $table->foreign('clasification_id')->references('id')->on('clasifications')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gens_results');
    }
}
