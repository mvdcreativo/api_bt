<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTracingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('patient_id');
            $table->boolean('mas_uni')->nullable();
            $table->date('mas_uni_date')->nullable();
            $table->string('mas_uni_obs')->nullable();
            $table->boolean('mas_bi')->nullable();
            $table->date('mas_bi_date')->nullable();
            $table->string('mas_bi_obs')->nullable();
            $table->boolean('oof')->nullable();
            $table->date('oof_date')->nullable();
            $table->string('oof_obs')->nullable();
            $table->boolean('hist')->nullable();
            $table->date('hist_date')->nullable();
            $table->string('hist_obs')->nullable();
            $table->boolean('gast')->nullable();
            $table->date('gast_date')->nullable();
            $table->string('gast_obs')->nullable();
            $table->boolean('qui')->nullable();
            $table->date('qui_date')->nullable();
            $table->string('qui_obs')->nullable();
            $table->boolean('onc')->nullable();
            $table->date('onc_date')->nullable();
            $table->string('onc_obs')->nullable();


            $table->foreign('patient_id')->references('id')->on('patients')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracings');
    }
}
