<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('families', function (Blueprint $table) {
            $table->id();
            $table->string('code',50);
            $table->boolean('studied');
            $table->unsignedBigInteger('dg_id');
            $table->unsignedBigInteger('consulta_cgu_id');
            $table->timestamps();

            $table->foreign('dg_id')->references('id')->on('dgs')->onDelete('restrict');
            $table->foreign('consulta_cgu_id')->references('id')->on('consulta_cgus')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('families');
    }
}
