<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('patient_id');
            $table->unsignedBigInteger('sample_id');
            $table->unsignedBigInteger('financing_id')->nullable();
            $table->unsignedBigInteger('lab_id')->nullable();
            $table->unsignedBigInteger('study_type_id')->nullable();
            $table->unsignedBigInteger('result_study_type_id')->nullable();
            $table->date('date_sol')->nullable();
            $table->date('date_recep')->nullable();
            $table->date('date_entrega')->nullable();
            $table->string('obs_result')->nullable();
            $table->boolean('reclasificado')->nullable();
            $table->string('reclas_obs')->nullable();

            $table->timestamps();

            $table->foreign('patient_id')->references('id')->on('patients')->onDelete('cascade');
            $table->foreign('sample_id')->references('id')->on('samples')->onDelete('cascade');
            $table->foreign('financing_id')->references('id')->on('financings')->onDelete('cascade');
            $table->foreign('study_type_id')->references('id')->on('study_types')->onDelete('cascade');
            $table->foreign('result_study_type_id')->references('id')->on('result_study_types')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
