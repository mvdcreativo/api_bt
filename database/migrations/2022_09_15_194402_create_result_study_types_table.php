<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultStudyTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_study_types', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('study_type_id');
            $table->string('name');
            $table->timestamps();

            $table->foreign('study_type_id')->references('id')->on('study_types')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_study_types');
    }
}
