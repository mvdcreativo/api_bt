<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('type_location_id');
            $table->string('code');
            $table->integer('racks');
            $table->integer('boxes');
            $table->integer('positions');
            $table->longText('description')->nullable();
            $table->string('brand_model')->nullable();
            $table->string('capacity')->nullable();
            $table->timestamps();

            $table->foreign('type_location_id')->references('id')->on('type_locations')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
